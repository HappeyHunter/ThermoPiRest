package com.dromree.thermopi

import com.dromree.thermopi.gpio.simulation.SimulatedGpioController
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.pi4j.io.gpio.GpioController
import com.pi4j.io.gpio.GpioFactory
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
class ThermoPiConfiguration
constructor(private val userDetailsService: UserDetailsService) {

    private val logger = LoggerFactory.getLogger(ThermoPiConfiguration::class.java.name)

    @Value("\${hash.rounds:7}")
    private val hashRounds: Int = 0

    @Bean
    fun getGpioController(): GpioController {
        return if (ThermoPiConstants.ARM_ARCH.equals(System.getProperty(ThermoPiConstants.OS_ARCH), ignoreCase = true)) {
            GpioFactory.getInstance()
        } else {
            logger.debug("Non-ARM architecture detected. Simulating GPIO.")
            SimulatedGpioController()
        }
    }

    @Bean
    fun getObjectMapper(): ObjectMapper {
        val objectMapper = ObjectMapper()

        objectMapper.registerModule(JavaTimeModule())
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

        return objectMapper
    }

    @Bean
    fun authProvider(): DaoAuthenticationProvider {
        val authProvider = DaoAuthenticationProvider()
        authProvider.setUserDetailsService(userDetailsService)
        authProvider.setPasswordEncoder(passwordEncoder())
        return authProvider
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder(hashRounds)
    }

}