package com.dromree.thermopi.boiler

import com.dromree.thermopi.gpio.GPIOAccess
import com.dromree.thermopi.rest.data.DayScheduleData
import com.dromree.thermopi.rest.data.HeatingStatusData
import com.dromree.thermopi.services.*
import com.dromree.thermopi.util.ThermoPiUtils
import com.dromree.thermopi.util.aspects.annotations.TimeLogger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.LocalDateTime

/**
 * Manages control of the boiler.
 * Periodically checks the status from the database to see if heating should be enabled or disabled
 *
 */
@Component
class BoilerSignaler @Autowired
constructor(private val targetTemperatureService: TargetTemperatureService,
            private val temperatureRecordService: TemperatureRecordService,
            private val boostServices: BoostServices,
            private val holidayServices: HolidayServices,
            private val heatingScheduleServices: HeatingScheduleServices,
            private val heatingStatusServices: HeatingStatusServices,
            private val gpio: GPIOAccess) {

    @Value("\${temp.activation.difference:2}")
    private val temperatureActivationDifference: Int = 0


    /**
     * Checks if the heating should be enabled or disabled based on the state of the database
     *
     */
    @TimeLogger
    @Scheduled(cron = "\${cron.boiler:30 */2 * * * ?}")
    fun updateActiveStatus() {
        logger.debug("Updating active status")

        var enableHeating = false

        // get temperatures - target/current
        val targetTemperatureData = targetTemperatureService.targetTemperature

        val currentTemperature = temperatureRecordService.currentTemperature

        val latestHeatingStatus = heatingStatusServices.latestHeatingStatus

        val currentlyEnabled = latestHeatingStatus.isPresent && latestHeatingStatus.get().enabled

        if (targetTemperatureData.isPresent && currentTemperature.isPresent) {
            enableHeating = if (currentlyEnabled)
                currentTemperature.get().recentAverage < targetTemperatureData.get().temperature
            else
                currentTemperature.get().recentAverage < targetTemperatureData.get().temperature - temperatureActivationDifference
        }

        updateSignals(canActivateHeating(), enableHeating)
    }

    /**
     * Updates the GPIO signals and adds the new status to the database
     *
     * @param canBeActive   if the heating can be active i.e. scheduled or boosted. Controls the valve.
     * @param enableHeating if the heating will be activated if allowed. Controls the boiler.
     */
    private fun updateSignals(canBeActive: Boolean, enableHeating: Boolean) {
        val enable: Boolean

        if (canBeActive) {
            // set valve to active
            gpio.setValveState(true)

            logger.debug("Heating scheduled. Valve enabled")
            enable = if (enableHeating) {
                // set heating to high
                logger.debug("Enable heating")
                true
            } else {
                // set heating to low
                logger.debug("Disable heating")
                false
            }
        } else {
            gpio.setValveState(false)

            // set valve to low
            logger.debug("Disable heating")
            enable = false
        }

        // update the signal
        gpio.setBoilerState(enable)

        heatingStatusServices.setHeatingStatus(HeatingStatusData(enable))
    }

    /**
     * Returns true if the heating is either scheduled or boosted
     *
     * @return true if the heating is scheduled or boosted
     */
    private fun canActivateHeating(): Boolean {
        var canBeActive = false

        // get boost
        val currentBoostSetting = boostServices.latestBoostSetting

        if (currentBoostSetting.isPresent) {
            canBeActive = currentBoostSetting.get().enabled && ThermoPiUtils.isInFuture(currentBoostSetting.get().endDate!!)
        }

        if (!canBeActive) {
            // get holidays
            val currentHolidaysCount = holidayServices.currentHolidaysCount

            if (currentHolidaysCount == 0L) {
                // get schedule
                val now = LocalDateTime.now()
                val dayScheduleData = heatingScheduleServices.getScheduleByDay(now.monthValue, now.dayOfWeek.name)

                if (dayScheduleData.isPresent) {
                    canBeActive = isScheduled(dayScheduleData.get())
                }
            }
        }

        return canBeActive
    }

    /**
     * Returns true if the heating is scheduled
     *
     * @param dayScheduleData   schedule data for the current day
     * @return                  true if the heating is scheduled for the current time in the day provided
     */
    private fun isScheduled(dayScheduleData: DayScheduleData): Boolean {
        val now = LocalDateTime.now()
        val hour = now.hour.toString()

        return dayScheduleData.hours[hour]?.quarters?.get(getCurrentQuarter(now.minute))?.enabled!!
    }

    /**
     * Gets the name of the quarter the minute belongs to
     *
     * @param minute    the current minute of the hour
     * @return          the quarter the minute falls into
     */
    private fun getCurrentQuarter(minute: Int): String {
        return (minute / 15).toString()
    }

    companion object {

        private val logger = LoggerFactory.getLogger(BoilerSignaler::class.java.name)
    }
}
