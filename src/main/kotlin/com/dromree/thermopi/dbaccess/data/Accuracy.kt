package com.dromree.thermopi.dbaccess.data

enum class Accuracy {
    MASSIVE_OVERSHOT,   // more than 15 mins too late
    OVERSHOT,           // within 15 mins but too late
    ON_TARGET,          // within 5 mins
    UNDERSHOT,          // within 15 mins but too early
    MASSIVE_UNDERSHOT   // more than 15 mins too early
}