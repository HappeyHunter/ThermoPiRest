package com.dromree.thermopi.dbaccess.data

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

/**
 * DB Entity for Boost
 *
 */
@Document(collection = "BoostSetting")
class Boost(var enabled: Boolean,
            var endDate: LocalDateTime?,
            @Indexed
            var timestamp: LocalDateTime) {
    @Id
    var id: String? = null
}
