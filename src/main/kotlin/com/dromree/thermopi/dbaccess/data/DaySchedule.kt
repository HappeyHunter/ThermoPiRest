package com.dromree.thermopi.dbaccess.data

/**
 * DB Entity for Day Schedule of the Week Schedule
 *
 */
class DaySchedule(var hours: MutableMap<String, HourSchedule>)