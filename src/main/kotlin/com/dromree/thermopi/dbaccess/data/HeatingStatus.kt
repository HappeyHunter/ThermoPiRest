package com.dromree.thermopi.dbaccess.data

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

/**
 * DB Entity for the HeatingStatus
 */
@Document(collection = "HeatingStatus")
class HeatingStatus(var active: Boolean,
                    var boosted: Boolean?,
                    @Indexed
                    var startTime: LocalDateTime,
                    var startTemp: Double,
                    var targetTemp: Double,
                    var endTime: LocalDateTime?,
                    var endTemp: Double?,
                    var prediction: LocalDateTime?,
                    var predictionAccuracy: Accuracy?) {

    @Id
    var id: String? = null

}
