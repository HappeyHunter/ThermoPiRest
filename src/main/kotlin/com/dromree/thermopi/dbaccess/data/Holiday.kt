package com.dromree.thermopi.dbaccess.data

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

/**
 * DB Entity for the Holidays
 */
@Document(collection = "Holidays")
class Holiday(@Indexed
              var holidayId: String,
              var startDate: LocalDate,
              @Indexed
              var endDate: LocalDate) {

    @Id
    var id: String? = null

}
