package com.dromree.thermopi.dbaccess.data

/**
 * DB Entity for Hour Schedule of the Day Schedule
 *
 */
class HourSchedule(var quarters: MutableMap<String, QuarterSchedule>)
