package com.dromree.thermopi.dbaccess.data

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

/**
 * DB Entity for the PredictionValues
 */
@Document(collection = "PredictionValues")
class PredictionValues(@Indexed
                       var calculationDate: LocalDate,
                       var degreeVariable: Double) {

    @Id
    var id: String? = null
}
