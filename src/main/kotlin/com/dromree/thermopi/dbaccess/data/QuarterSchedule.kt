package com.dromree.thermopi.dbaccess.data

/**
 * DB Entity for Quarter Schedule of the Hour Schedule
 *
 */
class QuarterSchedule(var enabled: Boolean)