package com.dromree.thermopi.dbaccess.data

/**
 * DB Entity for the SensorReading of the TemperatureLog
 */
class SensorReading(var temperature: Double,
                    var humidity: Double)
