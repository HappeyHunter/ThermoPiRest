package com.dromree.thermopi.dbaccess.data

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

/**
 * DB Entity for the TargetTemperature
 */
@Document(collection = "TargetTemperature")
class TargetTemperature(var temperature: Double) {

    @Id
    var id: String? = null

}
