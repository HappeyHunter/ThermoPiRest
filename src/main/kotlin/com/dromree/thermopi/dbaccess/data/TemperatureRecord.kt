package com.dromree.thermopi.dbaccess.data

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.CompoundIndex
import org.springframework.data.mongodb.core.index.CompoundIndexes
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

/**
 * DB Entity for the TemperatureLog
 */
@Document(collection = "TemperatureLog")
@CompoundIndexes(CompoundIndex(name = "year_month_day", def = "{'year' : -1, 'month' : -1, 'day' : -1}"))
class TemperatureRecord(var year: Int,
                        var month: Int,
                        var day: Int,
                        var lastReadingTimestamp: LocalDateTime?,
                        var recentAverage: Double,
                        var maxTemp: Double,
                        var minTemp: Double,
                        var minutes: MutableMap<Int, SensorReading>?) {

    @Id
    var id: String? = null

}
