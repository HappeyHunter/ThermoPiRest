package com.dromree.thermopi.dbaccess.data

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

@Document(collection = "ThermoPiUserDetails")
class ThermoPiUserDetails(@Indexed
                          private var username: String,
                          private var password: String,
                          var accountExpired: Boolean,
                          var accountLocked: Boolean,
                          var credentialsExpired: Boolean,
                          var enabled: Boolean,
                          var roles: List<String>) : UserDetails {

    @Id
    var id: String? = null

    override fun getUsername(): String? {
        return username
    }

    fun setUsername(username: String) {
        this.username = username
    }

    override fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String) {
        this.password = password
    }

    override fun getAuthorities(): Collection<GrantedAuthority> {
        val authorities = ArrayList<GrantedAuthority>()

        roles.forEach { role -> authorities.add(SimpleGrantedAuthority("ROLE_$role")) }

        return authorities
    }

    override fun isAccountNonExpired(): Boolean {
        return !accountExpired
    }

    override fun isAccountNonLocked(): Boolean {
        return !accountLocked
    }

    override fun isCredentialsNonExpired(): Boolean {
        return !credentialsExpired
    }

    override fun isEnabled(): Boolean {
        return enabled
    }
}
