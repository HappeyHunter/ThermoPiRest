package com.dromree.thermopi.dbaccess.data

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

/**
 * DB Entity for HeatingSchedule
 *
 */
@Document(collection = "HeatingSchedule")
open class WeekSchedule(@Indexed var month: Int, var days: MutableMap<String, DaySchedule>) {

    @Id
    var id: String? = null

}
