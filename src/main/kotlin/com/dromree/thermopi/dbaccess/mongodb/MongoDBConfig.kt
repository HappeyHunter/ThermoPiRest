package com.dromree.thermopi.dbaccess.mongodb

import com.mongodb.MongoClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.mongodb.config.AbstractMongoConfiguration
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

/**
 * Configuration for MongoDB
 */
@Configuration
@EnableMongoRepositories(basePackages = ["com.dromree.thermopi.dbaccess.mongodb.repositories"])
class MongoDBConfig : AbstractMongoConfiguration() {

    @Value("\${mongo.server:localhost}")
    private val server: String = "localhost"
    @Value("\${mongo.db.name:ThermoPi}")
    private val dbName: String? = null

    override fun getDatabaseName(): String {
        return dbName!!
    }

    override fun mongoClient(): MongoClient {
        return MongoClient(server)
    }

    @Primary
    @Bean(name = ["mongoTemplate"])
    @Throws(Exception::class)
    override fun mongoTemplate(): MongoTemplate {
        return super.mongoTemplate()
    }
}
