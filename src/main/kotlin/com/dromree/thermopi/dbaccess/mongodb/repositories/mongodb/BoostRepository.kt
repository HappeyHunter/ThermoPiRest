package com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb

import com.dromree.thermopi.dbaccess.data.Boost
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface BoostRepository : MongoRepository<Boost, String> {

    fun findTopByOrderByTimestampDesc(): Boost?

}
