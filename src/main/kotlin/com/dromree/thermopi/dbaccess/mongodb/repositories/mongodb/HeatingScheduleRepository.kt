package com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb

import com.dromree.thermopi.dbaccess.data.WeekSchedule
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.CachePut
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface HeatingScheduleRepository : MongoRepository<WeekSchedule, String> {

    @Cacheable("schedule")
    fun findWeekScheduleByMonth(month: Int): WeekSchedule?

    @CachePut("schedule")
    override fun <S : WeekSchedule> save(schedule: S): S

    @CacheEvict(value = ["schedule"], allEntries = true)
    override fun deleteAll()

}
