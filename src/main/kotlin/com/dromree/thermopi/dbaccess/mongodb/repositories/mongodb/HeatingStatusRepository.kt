package com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb

import com.dromree.thermopi.dbaccess.data.HeatingStatus
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface HeatingStatusRepository : MongoRepository<HeatingStatus, String> {

    fun findTopByOrderByStartTimeDesc(): HeatingStatus?

    fun findFirst10ByActiveAndPredictionAccuracyExistsOrderByStartTimeDesc(active: Boolean, exists: Boolean): List<HeatingStatus>

}