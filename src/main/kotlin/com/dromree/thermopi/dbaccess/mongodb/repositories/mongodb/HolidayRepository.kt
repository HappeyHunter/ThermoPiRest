package com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb

import com.dromree.thermopi.dbaccess.data.Holiday
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
interface HolidayRepository : MongoRepository<Holiday, String> {

    fun findByHolidayId(holidayId: String): Holiday?

    fun findHolidaysByEndDateGreaterThanEqualOrderByStartDateAsc(date: LocalDate): List<Holiday>

    fun deleteHolidayByHolidayId(holidayId: String)

    fun countHolidaysByStartDateLessThanEqualAndEndDateGreaterThanEqual(startDate: LocalDate, endDate: LocalDate): Long

}