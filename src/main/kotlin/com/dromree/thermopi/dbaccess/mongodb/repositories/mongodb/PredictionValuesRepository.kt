package com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb

import com.dromree.thermopi.dbaccess.data.PredictionValues
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface PredictionValuesRepository : MongoRepository<PredictionValues, String> {

    fun findFirstByOrderByCalculationDateDesc(): PredictionValues?
}