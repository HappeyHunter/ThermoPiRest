package com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb

import com.dromree.thermopi.dbaccess.data.TargetTemperature
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface TargetTemperatureRepository : MongoRepository<TargetTemperature, String>