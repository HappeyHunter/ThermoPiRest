package com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb

import com.dromree.thermopi.dbaccess.data.TemperatureRecord
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface TemperatureRecordRepository : MongoRepository<TemperatureRecord, String> {

    @Query(fields = "{ 'minutes' : 0 }")
    fun findFirstByYearAndMonthAndDay(year: Int, month: Int, day: Int): TemperatureRecord?

    @Query(fields = "{ 'minutes' : 0 }")
    fun findFirstByOrderByYearDescMonthDescDayDesc(): TemperatureRecord?

    fun findTop1ByOrderByYearDescMonthDescDayDesc(): TemperatureRecord?
}
