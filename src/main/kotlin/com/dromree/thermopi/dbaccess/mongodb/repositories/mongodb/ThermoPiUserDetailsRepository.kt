package com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb

import com.dromree.thermopi.dbaccess.data.ThermoPiUserDetails
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ThermoPiUserDetailsRepository : MongoRepository<ThermoPiUserDetails, String> {

    fun findFirstByUsername(username: String): ThermoPiUserDetails?

}
