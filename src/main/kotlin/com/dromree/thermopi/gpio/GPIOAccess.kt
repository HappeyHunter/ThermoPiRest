package com.dromree.thermopi.gpio

import com.pi4j.io.gpio.GpioController
import com.pi4j.io.gpio.GpioPinDigitalOutput
import com.pi4j.io.gpio.PinState
import com.pi4j.io.gpio.RaspiPin
import org.springframework.stereotype.Component

/**
 * Controls access to the GPIO pins
 *
 */
@Component
class GPIOAccess(gpio: GpioController) {

    private val boilerOutput: GpioPinDigitalOutput = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "BoilerOutput", PinState.LOW)
    private val valveOutput: GpioPinDigitalOutput = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, "ValveOutput", PinState.LOW)

    init {
        boilerOutput.setShutdownOptions(true, PinState.LOW)
        valveOutput.setShutdownOptions(true, PinState.LOW)
    }

    /**
     * Updates the boiler state if it is changing
     *
     * @param active    true if the boiler should be enabled
     */
    fun setBoilerState(active: Boolean) {
        if (PinState.LOW == boilerOutput.state && active) {
            boilerOutput.high()
        } else if (PinState.HIGH == boilerOutput.state && !active) {
            boilerOutput.low()
        }
    }

    /**
     * Updates the valve state if it is changing
     *
     * @param active    true if the valve should be opened
     */
    fun setValveState(active: Boolean) {
        if (PinState.LOW == valveOutput.state && active) {
            valveOutput.high()
        } else if (PinState.HIGH == valveOutput.state && !active) {
            valveOutput.low()
        }
    }

}
