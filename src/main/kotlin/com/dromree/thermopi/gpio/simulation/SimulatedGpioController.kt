package com.dromree.thermopi.gpio.simulation

import com.pi4j.io.gpio.*
import com.pi4j.io.gpio.event.GpioPinListener
import com.pi4j.io.gpio.trigger.GpioTrigger

/**
 * Simulated GPIO controller for use when the GPIO is not available (not a Raspberry Pi)
 * Very stripped down.
 */
class SimulatedGpioController : GpioController {

    override fun export(mode: PinMode, defaultState: PinState, vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun export(mode: PinMode, vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun isExported(vararg pin: GpioPin): Boolean {
        throw UnsupportedOperationException()
    }

    override fun unexport(vararg pin: Pin) {
        throw UnsupportedOperationException()
    }

    override fun unexport(vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun unexportAll() {
        throw UnsupportedOperationException()
    }

    override fun setMode(mode: PinMode, vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun getMode(pin: GpioPin): PinMode {
        throw UnsupportedOperationException()
    }

    override fun isMode(mode: PinMode, vararg pin: GpioPin): Boolean {
        throw UnsupportedOperationException()
    }

    override fun setPullResistance(resistance: PinPullResistance, vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun getPullResistance(pin: GpioPin): PinPullResistance {
        throw UnsupportedOperationException()
    }

    override fun isPullResistance(resistance: PinPullResistance, vararg pin: GpioPin): Boolean {
        throw UnsupportedOperationException()
    }

    override fun high(vararg pin: GpioPinDigitalOutput) {
        throw UnsupportedOperationException()
    }

    override fun isHigh(vararg pin: GpioPinDigital): Boolean {
        throw UnsupportedOperationException()
    }

    override fun low(vararg pin: GpioPinDigitalOutput) {
        throw UnsupportedOperationException()
    }

    override fun isLow(vararg pin: GpioPinDigital): Boolean {
        throw UnsupportedOperationException()
    }

    override fun setState(state: PinState, vararg pin: GpioPinDigitalOutput) {
        throw UnsupportedOperationException()
    }

    override fun setState(state: Boolean, vararg pin: GpioPinDigitalOutput) {
        throw UnsupportedOperationException()
    }

    override fun isState(state: PinState, vararg pin: GpioPinDigital): Boolean {
        throw UnsupportedOperationException()
    }

    override fun getState(pin: GpioPinDigital): PinState {
        throw UnsupportedOperationException()
    }

    override fun toggle(vararg pin: GpioPinDigitalOutput) {
        throw UnsupportedOperationException()
    }

    override fun pulse(milliseconds: Long, vararg pin: GpioPinDigitalOutput) {
        throw UnsupportedOperationException()
    }

    override fun setValue(value: Double, vararg pin: GpioPinAnalogOutput) {
        throw UnsupportedOperationException()
    }

    override fun getValue(pin: GpioPinAnalog): Double {
        throw UnsupportedOperationException()
    }

    override fun addListener(listener: GpioPinListener, vararg pin: GpioPinInput) {
        throw UnsupportedOperationException()
    }

    override fun addListener(listeners: Array<GpioPinListener>, vararg pin: GpioPinInput) {
        throw UnsupportedOperationException()
    }

    override fun removeListener(listener: GpioPinListener, vararg pin: GpioPinInput) {
        throw UnsupportedOperationException()
    }

    override fun removeListener(listeners: Array<GpioPinListener>, vararg pin: GpioPinInput) {
        throw UnsupportedOperationException()
    }

    override fun removeAllListeners() {
        throw UnsupportedOperationException()
    }

    override fun addTrigger(trigger: GpioTrigger, vararg pin: GpioPinInput) {
        throw UnsupportedOperationException()
    }

    override fun addTrigger(triggers: Array<GpioTrigger>, vararg pin: GpioPinInput) {
        throw UnsupportedOperationException()
    }

    override fun removeTrigger(trigger: GpioTrigger, vararg pin: GpioPinInput) {
        throw UnsupportedOperationException()
    }

    override fun removeTrigger(triggers: Array<GpioTrigger>, vararg pin: GpioPinInput) {
        throw UnsupportedOperationException()
    }

    override fun removeAllTriggers() {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalMultipurposePin(provider: GpioProvider, pin: Pin, name: String, mode: PinMode, resistance: PinPullResistance): GpioPinDigitalMultipurpose {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalMultipurposePin(provider: GpioProvider, pin: Pin, mode: PinMode, resistance: PinPullResistance): GpioPinDigitalMultipurpose {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalMultipurposePin(provider: GpioProvider, pin: Pin, name: String, mode: PinMode): GpioPinDigitalMultipurpose {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalMultipurposePin(provider: GpioProvider, pin: Pin, mode: PinMode): GpioPinDigitalMultipurpose {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalMultipurposePin(pin: Pin, name: String, mode: PinMode, resistance: PinPullResistance): GpioPinDigitalMultipurpose {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalMultipurposePin(pin: Pin, mode: PinMode, resistance: PinPullResistance): GpioPinDigitalMultipurpose {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalMultipurposePin(pin: Pin, name: String, mode: PinMode): GpioPinDigitalMultipurpose {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalMultipurposePin(pin: Pin, mode: PinMode): GpioPinDigitalMultipurpose {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalInputPin(provider: GpioProvider, pin: Pin, name: String, resistance: PinPullResistance): GpioPinDigitalInput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalInputPin(provider: GpioProvider, pin: Pin, resistance: PinPullResistance): GpioPinDigitalInput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalInputPin(provider: GpioProvider, pin: Pin, name: String): GpioPinDigitalInput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalInputPin(provider: GpioProvider, pin: Pin): GpioPinDigitalInput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalInputPin(pin: Pin, name: String, resistance: PinPullResistance): GpioPinDigitalInput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalInputPin(pin: Pin, resistance: PinPullResistance): GpioPinDigitalInput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalInputPin(pin: Pin, name: String): GpioPinDigitalInput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalInputPin(pin: Pin): GpioPinDigitalInput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalOutputPin(provider: GpioProvider, pin: Pin, name: String, defaultState: PinState): GpioPinDigitalOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalOutputPin(provider: GpioProvider, pin: Pin, defaultState: PinState): GpioPinDigitalOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalOutputPin(provider: GpioProvider, pin: Pin, name: String): GpioPinDigitalOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalOutputPin(provider: GpioProvider, pin: Pin): GpioPinDigitalOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalOutputPin(pin: Pin, name: String, defaultState: PinState): GpioPinDigitalOutput {
        return SimulatedGpioPinDigitalOutput(defaultState, pin, name)
    }

    override fun provisionDigitalOutputPin(pin: Pin, defaultState: PinState): GpioPinDigitalOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalOutputPin(pin: Pin, name: String): GpioPinDigitalOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionDigitalOutputPin(pin: Pin): GpioPinDigitalOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogInputPin(provider: GpioProvider, pin: Pin, name: String): GpioPinAnalogInput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogInputPin(provider: GpioProvider, pin: Pin): GpioPinAnalogInput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogInputPin(pin: Pin, name: String): GpioPinAnalogInput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogInputPin(pin: Pin): GpioPinAnalogInput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogOutputPin(provider: GpioProvider, pin: Pin, name: String, defaultValue: Double): GpioPinAnalogOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogOutputPin(provider: GpioProvider, pin: Pin, defaultValue: Double): GpioPinAnalogOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogOutputPin(provider: GpioProvider, pin: Pin, name: String): GpioPinAnalogOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogOutputPin(provider: GpioProvider, pin: Pin): GpioPinAnalogOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogOutputPin(pin: Pin, name: String, defaultValue: Double): GpioPinAnalogOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogOutputPin(pin: Pin, defaultValue: Double): GpioPinAnalogOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogOutputPin(pin: Pin, name: String): GpioPinAnalogOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionAnalogOutputPin(pin: Pin): GpioPinAnalogOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionPwmOutputPin(provider: GpioProvider, pin: Pin, name: String, defaultValue: Int): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionPwmOutputPin(provider: GpioProvider, pin: Pin, defaultValue: Int): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionPwmOutputPin(provider: GpioProvider, pin: Pin, name: String): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionPwmOutputPin(provider: GpioProvider, pin: Pin): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionPwmOutputPin(pin: Pin, name: String, defaultValue: Int): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionPwmOutputPin(pin: Pin, defaultValue: Int): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionPwmOutputPin(pin: Pin, name: String): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionPwmOutputPin(pin: Pin): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionSoftPwmOutputPin(provider: GpioProvider, pin: Pin, name: String, defaultValue: Int): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionSoftPwmOutputPin(provider: GpioProvider, pin: Pin, defaultValue: Int): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionSoftPwmOutputPin(provider: GpioProvider, pin: Pin, name: String): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionSoftPwmOutputPin(provider: GpioProvider, pin: Pin): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionSoftPwmOutputPin(pin: Pin, name: String, defaultValue: Int): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionSoftPwmOutputPin(pin: Pin, defaultValue: Int): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionSoftPwmOutputPin(pin: Pin, name: String): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionSoftPwmOutputPin(pin: Pin): GpioPinPwmOutput {
        throw UnsupportedOperationException()
    }

    override fun provisionPin(provider: GpioProvider, pin: Pin, name: String, mode: PinMode, defaultState: PinState): GpioPin {
        throw UnsupportedOperationException()
    }

    override fun provisionPin(provider: GpioProvider, pin: Pin, name: String, mode: PinMode): GpioPin {
        throw UnsupportedOperationException()
    }

    override fun provisionPin(provider: GpioProvider, pin: Pin, mode: PinMode): GpioPin {
        throw UnsupportedOperationException()
    }

    override fun provisionPin(pin: Pin, name: String, mode: PinMode): GpioPin {
        throw UnsupportedOperationException()
    }

    override fun provisionPin(pin: Pin, mode: PinMode): GpioPin {
        throw UnsupportedOperationException()
    }

    override fun setShutdownOptions(options: GpioPinShutdown, vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun setShutdownOptions(unexport: Boolean?, vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun setShutdownOptions(unexport: Boolean?, state: PinState, vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun setShutdownOptions(unexport: Boolean?, state: PinState, resistance: PinPullResistance, vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun setShutdownOptions(unexport: Boolean?, state: PinState, resistance: PinPullResistance, mode: PinMode, vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun getProvisionedPins(): Collection<GpioPin> {
        throw UnsupportedOperationException()
    }

    override fun getProvisionedPin(pin: Pin): GpioPin {
        throw UnsupportedOperationException()
    }

    override fun getProvisionedPin(name: String): GpioPin {
        throw UnsupportedOperationException()
    }

    override fun unprovisionPin(vararg pin: GpioPin) {
        throw UnsupportedOperationException()
    }

    override fun isShutdown(): Boolean {
        throw UnsupportedOperationException()
    }

    override fun shutdown() {
        throw UnsupportedOperationException()
    }
}
