package com.dromree.thermopi.gpio.simulation

import com.pi4j.io.gpio.*
import com.pi4j.io.gpio.event.GpioPinListener
import java.util.concurrent.Callable
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

/**
 * Simulated GPIO output for use when the GPIO is not available (not a Raspberry Pi)
 * Very stripped down.
 */
class SimulatedGpioPinDigitalOutput(private var currentState: PinState?, private val pin: Pin, private var name: String?) : GpioPinDigitalOutput {
    override fun blink(delay: Long, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun blink(delay: Long, blinkState: PinState?, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun blink(delay: Long, duration: Long, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun blink(delay: Long, duration: Long, blinkState: PinState?, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, callback: Callable<Void>?, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, blocking: Boolean, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, blocking: Boolean, callback: Callable<Void>?, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, pulseState: PinState?, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, pulseState: PinState?, callback: Callable<Void>?, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, pulseState: PinState?, blocking: Boolean, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, pulseState: PinState?, blocking: Boolean, callback: Callable<Void>?, timeUnit: TimeUnit?): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun high() {
        currentState = PinState.HIGH
    }

    override fun low() {
        currentState = PinState.LOW
    }

    override fun toggle() {
        currentState = if (PinState.LOW == currentState) PinState.HIGH else PinState.LOW
    }

    override fun blink(delay: Long): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun blink(delay: Long, blinkState: PinState): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun blink(delay: Long, duration: Long): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun blink(delay: Long, duration: Long, blinkState: PinState): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, callback: Callable<Void>): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, blocking: Boolean): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, blocking: Boolean, callback: Callable<Void>): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, pulseState: PinState): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, pulseState: PinState, callback: Callable<Void>): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, pulseState: PinState, blocking: Boolean): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun pulse(duration: Long, pulseState: PinState, blocking: Boolean, callback: Callable<Void>): Future<*> {
        throw UnsupportedOperationException()
    }

    override fun setState(state: PinState) {
        currentState = state
    }

    override fun setState(state: Boolean) {
        currentState = if (state) PinState.HIGH else PinState.LOW
    }

    override fun isHigh(): Boolean {
        return PinState.HIGH == currentState
    }

    override fun isLow(): Boolean {
        return PinState.LOW == currentState
    }

    override fun getState(): PinState? {
        return currentState
    }

    override fun isState(state: PinState): Boolean {
        return currentState == state
    }

    override fun getProvider(): GpioProvider {
        throw UnsupportedOperationException()
    }

    override fun getPin(): Pin {
        return pin
    }

    override fun setName(name: String) {
        this.name = name
    }

    override fun getName(): String? {
        return name
    }

    override fun setTag(tag: Any) {
        throw UnsupportedOperationException()
    }

    override fun getTag(): Any {
        throw UnsupportedOperationException()
    }

    override fun setProperty(key: String, value: String) {
        throw UnsupportedOperationException()
    }

    override fun hasProperty(key: String): Boolean {
        throw UnsupportedOperationException()
    }

    override fun getProperty(key: String): String {
        throw UnsupportedOperationException()
    }

    override fun getProperty(key: String, defaultValue: String): String {
        throw UnsupportedOperationException()
    }

    override fun getProperties(): Map<String, String> {
        throw UnsupportedOperationException()
    }

    override fun removeProperty(key: String) {
        throw UnsupportedOperationException()
    }

    override fun clearProperties() {
        throw UnsupportedOperationException()
    }

    override fun export(mode: PinMode) {
        throw UnsupportedOperationException()
    }

    override fun export(mode: PinMode, defaultState: PinState) {
        throw UnsupportedOperationException()
    }

    override fun unexport() {
        throw UnsupportedOperationException()
    }

    override fun isExported(): Boolean {
        throw UnsupportedOperationException()
    }

    override fun setMode(mode: PinMode) {
        throw UnsupportedOperationException()
    }

    override fun getMode(): PinMode {
        return PinMode.DIGITAL_OUTPUT
    }

    override fun isMode(mode: PinMode): Boolean {
        return PinMode.DIGITAL_OUTPUT == mode
    }

    override fun setPullResistance(resistance: PinPullResistance) {
        throw UnsupportedOperationException()
    }

    override fun getPullResistance(): PinPullResistance {
        throw UnsupportedOperationException()
    }

    override fun isPullResistance(resistance: PinPullResistance): Boolean {
        throw UnsupportedOperationException()
    }

    override fun getListeners(): Collection<GpioPinListener> {
        throw UnsupportedOperationException()
    }

    override fun addListener(vararg listener: GpioPinListener) {
        throw UnsupportedOperationException()
    }

    override fun addListener(listeners: List<GpioPinListener>) {
        throw UnsupportedOperationException()
    }

    override fun hasListener(vararg listener: GpioPinListener): Boolean {
        throw UnsupportedOperationException()
    }

    override fun removeListener(vararg listener: GpioPinListener) {
        throw UnsupportedOperationException()
    }

    override fun removeListener(listeners: List<GpioPinListener>) {
        throw UnsupportedOperationException()
    }

    override fun removeAllListeners() {
        throw UnsupportedOperationException()
    }

    override fun getShutdownOptions(): GpioPinShutdown {
        throw UnsupportedOperationException()
    }

    override fun setShutdownOptions(options: GpioPinShutdown) {
        throw UnsupportedOperationException()
    }

    override fun setShutdownOptions(unexport: Boolean?) {
        throw UnsupportedOperationException()
    }

    override fun setShutdownOptions(unexport: Boolean?, state: PinState) {
        // Won't need to do anything since it's just a simulation
    }

    override fun setShutdownOptions(unexport: Boolean?, state: PinState, resistance: PinPullResistance) {
        throw UnsupportedOperationException()
    }

    override fun setShutdownOptions(unexport: Boolean?, state: PinState, resistance: PinPullResistance, mode: PinMode) {
        throw UnsupportedOperationException()
    }
}
