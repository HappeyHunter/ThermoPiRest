package com.dromree.thermopi.prediction

import com.dromree.thermopi.dbaccess.data.PredictionValues
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.HeatingStatusRepository
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.PredictionValuesRepository
import com.dromree.thermopi.util.aspects.annotations.TimeLogger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.temporal.ChronoUnit

@Component
class PredictionRecalculator(private val predictionValuesRepository: PredictionValuesRepository, private val heatingStatusRepository: HeatingStatusRepository) {

    companion object {
        private val logger = LoggerFactory.getLogger(PredictionRecalculator::class.java.name)
    }

    @TimeLogger
    @Scheduled(cron = "\${cron.prediction:0 0 4 * * ?}")
    fun recalculatePredictionValues() {
        // Simple calculation for now until I come up with something
        logger.debug("Recalculating prediction values")

        val latestStatuses = heatingStatusRepository.findFirst10ByActiveAndPredictionAccuracyExistsOrderByStartTimeDesc(true, true)
        var degreeVariable = 0.0

        if (!latestStatuses.isEmpty()) {

            for (phase in latestStatuses) {
                    val timeDiff = phase.startTime.until(phase.endTime, ChronoUnit.MINUTES)
                    val tempDiff = phase.targetTemp - phase.startTemp

                    degreeVariable += timeDiff / tempDiff
            }

            degreeVariable /= latestStatuses.size.toDouble()
        } else {
            degreeVariable = 1.0
        }

        val predictionValues = PredictionValues(LocalDate.now(), degreeVariable)

        predictionValuesRepository.save(predictionValues)
    }
}
