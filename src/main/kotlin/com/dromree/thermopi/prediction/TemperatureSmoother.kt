package com.dromree.thermopi.prediction

import com.dromree.thermopi.dbaccess.data.SensorReading
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.TemperatureRecordRepository
import com.dromree.thermopi.services.TemperatureRecordService
import com.dromree.thermopi.util.aspects.annotations.TimeLogger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class TemperatureSmoother(private val temperatureRecordRepository: TemperatureRecordRepository,
                          private val temperatureRecordService: TemperatureRecordService) {

    companion object {
        private val logger = LoggerFactory.getLogger(TemperatureSmoother::class.java.name)
    }

    @TimeLogger
    @Scheduled(cron = "\${cron.smoothing:0 */2 * * * ?}")
    fun calculateRecentAverage() {
        logger.debug("Nor prediction values")

        val currentLog = temperatureRecordRepository.findTop1ByOrderByYearDescMonthDescDayDesc()

        if(currentLog != null && currentLog.minutes!!.size > 1) {
            currentLog.minutes!!.entries.stream()
                    .sorted(Comparator.comparing(Map.Entry<Int, SensorReading>::key).reversed())
                    .limit(5)
                    .mapToDouble { entry -> entry.value.temperature }
                    .average()
                    .ifPresent {
                            recentAverage -> run {
                            temperatureRecordService.recordRecentAverage(currentLog.id, recentAverage)
                        }
                    }
        }
    }
}