package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.ZonedDateTime
import javax.validation.constraints.NotNull

/**
 * Network side data object for Boost
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
class BoostData
@JsonCreator constructor(@NotNull(message = "New boost state required for update")
                         @JsonProperty("enabled")
                         var enabled: Boolean,
                         @JsonProperty("endDate")
                         var endDate: ZonedDateTime?)