package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonInclude
import java.time.ZonedDateTime

/**
 * Network side data object for Current Status
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
object CurrentStatusData {
    var currentTemperature: Double? = null
    var targetTemperature: Double? = null
    var heatingEnabled: Boolean? = null
    var boostEnabled: Boolean? = null
    var temperatureTimestamp: ZonedDateTime? = null
}
