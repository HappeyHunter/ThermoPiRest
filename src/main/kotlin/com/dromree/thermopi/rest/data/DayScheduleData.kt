package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

/**
 * Network side data object for Day Schedule
 */
class DayScheduleData
@JsonCreator constructor(@NotEmpty(message = "Hour schedule is required")
                         @Size(min = 24, max = 24, message = "Schedule must include settings for 24 hours")
                         @JsonProperty("hours")
                         var hours: MutableMap<String, HourScheduleData>)