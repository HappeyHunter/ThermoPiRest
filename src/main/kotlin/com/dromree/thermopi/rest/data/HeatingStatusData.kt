package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonCreator

/**
 * Network side data object for Heating Status
 */
class HeatingStatusData
@JsonCreator constructor(var enabled: Boolean)