package com.dromree.thermopi.rest.data

import com.dromree.thermopi.rest.validation.HolidayDate
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate
import javax.validation.constraints.FutureOrPresent
import javax.validation.constraints.NotNull

/**
 * Network side data object for Holiday
 */
@HolidayDate
class HolidayData
@JsonCreator constructor(var holidayId: String?,
                         @NotNull(message = "Start Date is required")
                         @FutureOrPresent(message = "Start date must be today or in the future")
                         @JsonProperty("startDate")
                         var startDate: LocalDate,
                         @NotNull(message = "End Date is required")
                         @FutureOrPresent(message = "End date must be today or in the future")
                         @JsonProperty("endDate")
                         var endDate: LocalDate)