package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

/**
 * Network side data object for Hour Schedule
 */
class HourScheduleData
@JsonCreator constructor(@NotEmpty(message = "Quarter schedule is required")
                         @Size(min = 4, max = 4, message = "Quarter schedule must include settings for 4 quarters")
                         @JsonProperty("quarters")
                         var quarters: MutableMap<String, QuarterScheduleData>)