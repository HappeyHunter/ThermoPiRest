package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

/**
 * Network side data object for Quarter Schedule
 */
class QuarterScheduleData
@JsonCreator constructor(@NotNull(message = "Quarter state is required")
                         @JsonProperty("enabled")
                         var enabled: Boolean)