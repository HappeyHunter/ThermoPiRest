package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.PositiveOrZero

/**
 * Network side data object for Target Temperature
 */
class TargetTemperatureData
@JsonCreator constructor(@PositiveOrZero(message = "Target temperature cannot be negative")
                         @JsonProperty("temperature")
                         var temperature: Double)