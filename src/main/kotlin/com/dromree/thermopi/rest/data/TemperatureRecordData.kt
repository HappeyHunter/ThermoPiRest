package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

/**
 * Network side data object for Temperature Record
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
class TemperatureRecordData
@JsonCreator constructor(@NotNull(message = "Temperature is required for update")
                         @JsonProperty("temperature")
                         var temperature: Double,
                         @NotNull(message = "Humidity is required for update")
                         @JsonProperty("humidity")
                         var humidity: Double)