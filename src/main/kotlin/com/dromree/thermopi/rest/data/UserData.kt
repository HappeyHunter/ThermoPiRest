package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotEmpty

/**
 * Network side data object for Users
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
class UserData
@JsonCreator constructor(@NotEmpty(message = "Username cannot be empty")
                         @JsonProperty("username")
                         var username: String,
                         @NotEmpty(message = "Password cannot be empty")
                         @JsonProperty("password")
                         var password: String,
                         @NotEmpty(message = "New user must have at least 1 role")
                         @JsonProperty("roles")
                         var roles: List<String>)