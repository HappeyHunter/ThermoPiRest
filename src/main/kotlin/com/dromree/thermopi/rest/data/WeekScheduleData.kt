package com.dromree.thermopi.rest.data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.*

/**
 * Network side data object for Week Schedule
 */
class WeekScheduleData
@JsonCreator constructor(@NotNull
                         @Min(1)
                         @Max(12)
                         @JsonProperty("month")
                         var month: Int,
                         @NotEmpty(message = "Day schedule is required")
                         @Size(min = 7, max = 7, message = "Schedule must include settings for 7 days")
                         @JsonProperty("days")
                         var days: MutableMap<String, DayScheduleData>)