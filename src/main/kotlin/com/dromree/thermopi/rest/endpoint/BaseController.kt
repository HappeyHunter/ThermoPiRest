package com.dromree.thermopi.rest.endpoint

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

/**
 * Base REST controller with some handy response methods
 *
 */
abstract class BaseController {

    internal fun ok(): ResponseEntity<*> {
        return ResponseEntity<Any>(HttpStatus.OK)
    }

    internal fun ok(responseBody: Any): ResponseEntity<*> {
        return ResponseEntity(responseBody, HttpStatus.OK)
    }

    internal fun notFound(): ResponseEntity<*> {
        return ResponseEntity<Any>(HttpStatus.NOT_FOUND)
    }

    internal fun noContent(): ResponseEntity<*> {
        return ResponseEntity<Any>(HttpStatus.NO_CONTENT)
    }

    internal fun conflict(): ResponseEntity<*> {
        return ResponseEntity<Any>(HttpStatus.CONFLICT)
    }

    internal fun badRequest(): ResponseEntity<*> {
        return ResponseEntity<Any>(HttpStatus.BAD_REQUEST)
    }
}