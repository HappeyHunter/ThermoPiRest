package com.dromree.thermopi.rest.endpoint

import com.dromree.thermopi.rest.data.BoostData
import com.dromree.thermopi.services.BoostServices
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

/**
 * Rest Controller for Boost
 */
@RestController
@RequestMapping("ThermoPi/Boost")
@PreAuthorize("hasRole('USER')")
class BoostController(private val boostServices: BoostServices) : BaseController() {

    /**
     * Gets the latest boost setting
     *
     * @return  The latest boost setting
     */
    val boostSetting: ResponseEntity<*>
        @GetMapping
        get() {
            val returnData = boostServices.latestBoostSetting

            return if (returnData.isPresent)
                ok(returnData.get())
            else
                noContent()
        }

    /**
     * Updates the boost setting with the enabled state of the provided data and returns the new value.
     *
     * @param boostData A boost object with a valid enabled state
     * @return          The new boost setting with the end date populated if enabled
     */
    @PostMapping
    fun setBoostSetting(@RequestBody @Valid boostData: BoostData): ResponseEntity<*> {
        return ok(boostServices.setBoostSetting(boostData))
    }

    companion object {
        private val logger = LoggerFactory.getLogger(BoostController::class.java.name)
    }
}