package com.dromree.thermopi.rest.endpoint

import com.dromree.thermopi.rest.data.CurrentStatusData
import com.dromree.thermopi.services.BoostServices
import com.dromree.thermopi.services.HeatingStatusServices
import com.dromree.thermopi.services.TargetTemperatureService
import com.dromree.thermopi.services.TemperatureRecordService
import com.dromree.thermopi.util.ThermoPiUtils
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Rest Controller for Current Status
 */
@RestController
@RequestMapping("/ThermoPi/CurrentStatus")
@PreAuthorize("hasRole('USER')")
class CurrentStatusController(private val heatingStatusServices: HeatingStatusServices, private val boostServices: BoostServices, private val temperatureRecordService: TemperatureRecordService, private val targetTemperatureService: TargetTemperatureService) : BaseController() {

    /**
     * Gets the current status of the system.
     *
     * @return  Status containing the active status, boost setting, current temperature, and target temperature.
     */
    val status: ResponseEntity<*>
        @GetMapping
        get() {
            val currentStatus = CurrentStatusData

            val heatingStatusData = heatingStatusServices.latestHeatingStatus

            val boostData = boostServices.latestBoostSetting
            val temperatureRecordData = temperatureRecordService.currentTemperature
            val targetTemperatureData = targetTemperatureService.targetTemperature

            if (!boostData.isPresent
                    || !temperatureRecordData.isPresent
                    || !targetTemperatureData.isPresent) {
                return noContent()
            }

            if (heatingStatusData.isPresent) {
                currentStatus.heatingEnabled = heatingStatusData.get().enabled
            } else {
                currentStatus.heatingEnabled = false
            }

            currentStatus.boostEnabled = boostData.get().enabled && boostData.get().endDate != null && ThermoPiUtils.isInFuture(boostData.get().endDate!!)

            currentStatus.currentTemperature = temperatureRecordData.get().recentAverage
            currentStatus.targetTemperature = targetTemperatureData.get().temperature
            currentStatus.temperatureTimestamp = ThermoPiUtils.localToZoned(temperatureRecordData.get().lastReadingTimestamp)

            return ok(currentStatus)
        }

    companion object {
        private val logger = LoggerFactory.getLogger(CurrentStatusController::class.java.name)
    }
}
