package com.dromree.thermopi.rest.endpoint

import com.dromree.thermopi.rest.data.TemperatureRecordData
import com.dromree.thermopi.services.TemperatureRecordService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

/**
 * Rest Controller for Current Temperature
 */
@RestController
@RequestMapping("/ThermoPi/Secure/CurrentTemperature")
class CurrentTemperatureController(private val temperatureRecordService: TemperatureRecordService) : BaseController() {

    /**
     * Updates the current temperature with the provided value
     *
     * @param currentData   the new current temperature
     */
    @PutMapping
    fun putCurrentTemperature(@RequestBody @Valid currentData: TemperatureRecordData): ResponseEntity<*> {
        temperatureRecordService.recordCurrentTemperature(currentData)

        return ok()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(CurrentTemperatureController::class.java.name)
    }
}
