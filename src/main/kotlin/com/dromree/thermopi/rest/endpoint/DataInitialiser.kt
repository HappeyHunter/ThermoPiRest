package com.dromree.thermopi.rest.endpoint

import com.dromree.thermopi.rest.data.*
import com.dromree.thermopi.services.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/ThermoPi/Secure/DataInitialiser")
class DataInitialiser @Autowired
constructor(private val heatingStatusServices: HeatingStatusServices, private val boostServices: BoostServices, private val temperatureRecordService: TemperatureRecordService, private val targetTemperatureService: TargetTemperatureService, private val heatingScheduleServices: HeatingScheduleServices, private val thermoPiUserDetailsService: ThermoPiUserDetailsService) : BaseController() {

    @PostMapping
    fun initialiseDBData(): ResponseEntity<*> {

        try {
            // Setup the boost setting to disabled
            // Setup the current temperature with dud data 100 degrees
            // Setup the target temperature with dud data 0 degrees
            // Setup the schedule for each month - All disabled
            createAdminUser()
            initBoost()
            initCurrentTemp()
            initTargetTemp()
            initHeatingStatus()
            heatingScheduleServices.initialiseHeatingSchedule()
        } catch (e: Exception) {
            logger.error("Error occurred inistalising database", e)
            return ResponseEntity(false, HttpStatus.INTERNAL_SERVER_ERROR)
        }

        // If all goes well return true

        return ok(true)
    }

    private fun createAdminUser() {
        val userData = UserData("admin", "password", Arrays.asList("ADMIN", "USER"))

        thermoPiUserDetailsService.addUser(userData)
    }

    private fun initHeatingStatus() {
        heatingStatusServices.setHeatingStatus(HeatingStatusData(false))
    }

    private fun initBoost() {
        val boostData = BoostData(false, null)

        boostServices.setBoostSetting(boostData)
    }

    private fun initCurrentTemp() {
        temperatureRecordService.recordCurrentTemperature(TemperatureRecordData(100.0, 100.0))
    }

    private fun initTargetTemp() {
        targetTemperatureService.setTargetTemperature(TargetTemperatureData(0.0))
    }

    companion object {

        private val logger = LoggerFactory.getLogger(DataInitialiser::class.java.name)
    }
}
