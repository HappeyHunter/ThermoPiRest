package com.dromree.thermopi.rest.endpoint

import com.dromree.thermopi.rest.data.HolidayData
import com.dromree.thermopi.services.HolidayServices
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

/**
 * Rest Controller for Holidays
 */
@RestController
@RequestMapping("/ThermoPi/Holidays")
@PreAuthorize("hasRole('USER')")
class HolidayController(private val holidayServices: HolidayServices) : BaseController() {

    /**
     * Gets a list of all current and future holidays
     *
     * @return  List of holidays or empty list if none are found.
     */
    val holidays: ResponseEntity<*>
        @GetMapping
        get() {
            val holidayDataList = holidayServices.currentAndFutureHolidays

            return ok(holidayDataList)
        }

    /**
     * Updates the holiday identified by the id
     *
     * @param holidayId     id of the holiday to be updated
     * @param aHolidayData  new state of the holiday
     */
    @PutMapping("/{holidayId}")
    fun putHoliday(@PathVariable holidayId: String, @RequestBody @Valid aHolidayData: HolidayData): ResponseEntity<*> {
        // Set the Id from the path
        aHolidayData.holidayId = holidayId

        holidayServices.updateHoliday(aHolidayData)

        return ok()
    }

    /**
     * Gets the holiday identified by the provided id
     *
     * @param holidayId id of the holiday to be retrieved
     * @return  The holiday with the provided id if found
     */
    @GetMapping("/{holidayId}")
    fun getHoliday(@PathVariable holidayId: String): ResponseEntity<*> {
        val holidayData = holidayServices.getHolidayByHolidayId(holidayId)

        return if (holidayData.isPresent)
            ok(holidayData.get())
        else
            notFound()
    }

    /**
     * Adds a new holiday using the provided holiday.
     * Generates a new id and returns the holiday with the id
     *
     * @param aHolidayData  new holiday to be added
     * @return              the holiday added and its generated id
     */
    @PostMapping
    fun addHoliday(@RequestBody @Valid aHolidayData: HolidayData): ResponseEntity<*> {
        holidayServices.addHoliday(aHolidayData)

        return ok(aHolidayData)
    }

    /**
     * Delete the holiday identified by the id
     *
     * @param holidayId id of the holiday to be deleted
     */
    @DeleteMapping("/{holidayId}")
    fun deleteHoliday(@PathVariable holidayId: String): ResponseEntity<*> {
        holidayServices.deleteHolidayByHolidayId(holidayId)

        return ok()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HolidayController::class.java.name)
    }
}
