package com.dromree.thermopi.rest.endpoint

import com.dromree.thermopi.rest.data.TargetTemperatureData
import com.dromree.thermopi.services.TargetTemperatureService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

/**
 * Rest Controller for Target Temperature
 */
@RestController
@RequestMapping("/ThermoPi/TargetTemperature")
@PreAuthorize("hasRole('USER')")
class TargetTemperatureController(private val targetTemperatureService: TargetTemperatureService) : BaseController() {

    /**
     * Gets the target temperature
     *
     * @return the target temperature
     */
    val targetTemperature: ResponseEntity<*>
        @GetMapping
        get() {
            val targetTemperatureData = targetTemperatureService.targetTemperature

            return if (targetTemperatureData.isPresent)
                ok(targetTemperatureData.get())
            else
                notFound()
        }

    /**
     * Update the target temperature to the provided value
     *
     * @param targetTemperatureData new target temperature
     */
    @PutMapping
    fun putTargetTemperature(@RequestBody @Valid targetTemperatureData: TargetTemperatureData): ResponseEntity<*> {
        targetTemperatureService.setTargetTemperature(targetTemperatureData)

        return ok()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(TargetTemperatureController::class.java.name)
    }

}
