package com.dromree.thermopi.rest.endpoint

import com.dromree.thermopi.rest.data.UserData
import com.dromree.thermopi.services.ThermoPiUserDetailsService
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid

@RestController
@RequestMapping("/ThermoPi/Users")
class UserController(private val userDetailsService: ThermoPiUserDetailsService) : BaseController() {

    @PutMapping("/update-password")
    @PreAuthorize("isFullyAuthenticated()")
    fun updatePassword(principal: Principal, @RequestBody newPassword: String): ResponseEntity<*> {
        return if (userDetailsService.updatePassword(principal.name, newPassword).isPresent)
            ok()
        else
            badRequest()
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    fun addUser(@RequestBody @Valid user: UserData): ResponseEntity<*> {

        return if (userDetailsService.addUser(user).isPresent)
            ok()
        else
            conflict()
    }

}