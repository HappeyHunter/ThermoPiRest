package com.dromree.thermopi.rest.endpoint

import com.dromree.thermopi.rest.data.DayScheduleData
import com.dromree.thermopi.rest.data.WeekScheduleData
import com.dromree.thermopi.services.HeatingScheduleServices
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

/**
 * Rest Controller for Weekly Schedule
 */
@RestController
@RequestMapping("/ThermoPi/WeeklySchedule")
@PreAuthorize("hasRole('USER')")
class WeeklyScheduleController(private val heatingScheduleServices: HeatingScheduleServices) : BaseController() {

    /**
     * Updates the identified month with the provided state
     *
     * @param month                 index of the month to be updated
     * @param weeklyScheduleData    new state of the month
     */
    @PutMapping("/{month}")
    fun putWeeklySchedule(@PathVariable month: Int, @RequestBody @Valid weeklyScheduleData: WeekScheduleData): ResponseEntity<*> {
        weeklyScheduleData.month = month

        return if (heatingScheduleServices.updateHeatingScheduleByWeek(weeklyScheduleData))
            ok()
        else
            notFound()
    }

    /**
     * Updates the identified day with the provided state
     *
     * @param month           index of the month to be updated
     * @param day             name of the day to be updated
     * @param dayScheduleData new state of the day
     */
    @PutMapping("/{month}/{day}")
    fun putDailySchedule(@PathVariable month: Int, @PathVariable day: String, @RequestBody @Valid dayScheduleData: DayScheduleData): ResponseEntity<*> {
        return if (heatingScheduleServices.updateHeatingScheduleByDay(month, day, dayScheduleData))
            ok()
        else
            notFound()
    }

    /**
     * Gets the month identified
     *
     * @param month index of the month to be rretireved
     * @return      data for the month provided
     */
    @GetMapping("/{month}")
    fun getWeeklySchedule(@PathVariable month: Int): ResponseEntity<*> {
        val weekScheduleData = heatingScheduleServices.getScheduleByMonth(month)

        return if (weekScheduleData.isPresent)
            ok(weekScheduleData.get())
        else
            notFound()
    }

    /**
     * Gets the day identified
     *
     * @param month index of the month to be retrieved
     * @param day   name of the day to be retrieved
     * @return      data for the day provided
     */
    @GetMapping("{month}/{day}")
    fun getDailySchedule(@PathVariable month: Int, @PathVariable day: String): ResponseEntity<*> {
        val dayScheduleData = heatingScheduleServices.getScheduleByDay(month, day)

        return if (dayScheduleData.isPresent)
            ok(dayScheduleData.get())
        else
            notFound()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(WeeklyScheduleController::class.java.name)
    }

}
