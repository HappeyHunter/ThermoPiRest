package com.dromree.thermopi.rest.security

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler
import java.net.Inet4Address
import java.net.InterfaceAddress
import java.net.NetworkInterface

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfiguration : WebSecurityConfigurerAdapter() {

    @Value("\${network.subnet:192.168.1.0/24}")
    private val subnet: String = ""

    /**
     * Configures the security settings.
     * Secure paths can only be accessed from the current network
     *
     * @param http          HTTP Security object
     * @throws Exception    Throws if something goes wrong with security setup
     */
    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        val localSubnet = getSubnet()

        http.csrf().disable()
                .exceptionHandling()//.authenticationEntryPoint(Http403ForbiddenEntryPoint())
                .and()
                .authorizeRequests()
                .antMatchers("/ThermoPi/Secure/**", "/ThermoPi/Web/**")
                .access("hasIpAddress('127.0.0.1') or hasIpAddress('::1') or hasIpAddress('$localSubnet')")
                .antMatchers("/ThermoPi/**").fullyAuthenticated()
                .and()
                .formLogin()
                .loginPage(THERMO_PI_LOGIN_PAGE)
                .loginProcessingUrl("/ThermoPi/Login")
                .defaultSuccessUrl("/ThermoPi/Web/main.html", true)
                .failureHandler(SimpleUrlAuthenticationFailureHandler())
                .failureUrl(THERMO_PI_LOGIN_PAGE)
                .and()
                .logout()
                .logoutSuccessUrl(THERMO_PI_LOGIN_PAGE)

    }

    private fun getSubnet() : String {
        val localHost = Inet4Address.getLocalHost()
        val networkInterface = NetworkInterface.getByInetAddress(localHost)
        val interfaceAddressMaybe = networkInterface.interfaceAddresses.stream().findFirst();

        return if (interfaceAddressMaybe.isPresent) buildSubnet(interfaceAddressMaybe.get()) else subnet
    }

    private fun buildSubnet(interfaceAddress: InterfaceAddress): String {
        return "${interfaceAddress.address.hostAddress}/${interfaceAddress.networkPrefixLength}"
    }

    companion object {
        const val THERMO_PI_LOGIN_PAGE = "/ThermoPi/Web/login.html"
    }

}