package com.dromree.thermopi.rest.validation

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

@Constraint(validatedBy = [HolidayDateValidator::class])
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Retention
annotation class HolidayDate(val message: String = "", val groups: Array<KClass<*>> = [], val payload: Array<KClass<out Payload>> = [])