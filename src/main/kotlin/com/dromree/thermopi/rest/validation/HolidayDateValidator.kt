package com.dromree.thermopi.rest.validation

import com.dromree.thermopi.rest.data.HolidayData
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class HolidayDateValidator : ConstraintValidator<HolidayDate, HolidayData> {

    override fun isValid(aHolidayData: HolidayData, context: ConstraintValidatorContext): Boolean {
        if (aHolidayData.endDate.isBefore(aHolidayData.startDate)) {
            context.buildConstraintViolationWithTemplate("End Date cannot be before Start Date").addConstraintViolation()
            return false
        }

        return true
    }

}