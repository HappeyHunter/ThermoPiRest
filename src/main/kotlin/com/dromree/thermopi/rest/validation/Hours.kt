package com.dromree.thermopi.rest.validation

import javax.validation.Constraint
import javax.validation.Payload
import javax.validation.constraints.Pattern
import kotlin.reflect.KClass

@Pattern(regexp = "0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23", message = "Hour provided is not valid")
@Target(AnnotationTarget.TYPE)
@Retention
@Constraint(validatedBy = [])
annotation class Hours(val message: String = "", val groups: Array<KClass<*>> = [], val payload: Array<KClass<out Payload>> = [])