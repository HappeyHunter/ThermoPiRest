package com.dromree.thermopi.rest.validation

import javax.validation.Constraint
import javax.validation.Payload
import javax.validation.constraints.Pattern
import kotlin.reflect.KClass

@Pattern(regexp = "[0123]", message = "Quarter provided is not valid")
@Target(AnnotationTarget.TYPE)
@Retention
@Constraint(validatedBy = [])
annotation class Quarters(val message: String = "", val groups: Array<KClass<*>> = [], val payload: Array<KClass<out Payload>> = [])