package com.dromree.thermopi.services

import com.dromree.thermopi.dbaccess.data.Boost
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.BoostRepository
import com.dromree.thermopi.rest.data.BoostData
import com.dromree.thermopi.util.ThermoPiUtils
import com.dromree.thermopi.util.aspects.annotations.TimeLogger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*

@Service
class BoostServices @Autowired
constructor(private val boostRepository: BoostRepository) {

    /**
     * Gets the latest boost setting
     *
     * @return  The latest boost setting if found, null otherwise
     */
    val latestBoostSetting: Optional<BoostData>
        @TimeLogger
        get() {
            val boostData = boostRepository.findTopByOrderByTimestampDesc()

            return if (boostData != null)
                Optional.of(convertDBToNetworkData(boostData))
            else
                Optional.empty()
        }

    /**
     * Converts a db Boost to a network Boost
     *
     * @param dbData    Boost to be converted
     * @return          network Boost
     */
    private fun convertDBToNetworkData(dbData: Boost): BoostData {
        return BoostData(dbData.enabled, ThermoPiUtils.localToZoned(dbData.endDate))
    }

    /**
     * Sets the boost setting and returns the new value.
     * Populates the end date if the boost is being enabled
     *
     * @param boostData     The boost data to be saved
     * @return              The boost data with the end date added
     */
    fun setBoostSetting(boostData: BoostData): BoostData {
        if (boostData.enabled) {
            // Trying to enable boost, add an end time
            boostData.endDate = ZonedDateTime.now().plusHours(1)
        }

        val newBoost = Boost(boostData.enabled, ThermoPiUtils.zonedToLocal(boostData.endDate), LocalDateTime.now())

        boostRepository.save(newBoost)

        return boostData
    }
}