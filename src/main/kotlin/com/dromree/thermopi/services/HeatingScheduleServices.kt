package com.dromree.thermopi.services

import com.dromree.thermopi.dbaccess.data.DaySchedule
import com.dromree.thermopi.dbaccess.data.HourSchedule
import com.dromree.thermopi.dbaccess.data.QuarterSchedule
import com.dromree.thermopi.dbaccess.data.WeekSchedule
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.HeatingScheduleRepository
import com.dromree.thermopi.rest.data.DayScheduleData
import com.dromree.thermopi.rest.data.WeekScheduleData
import com.dromree.thermopi.util.ScheduleConverter
import com.dromree.thermopi.util.aspects.annotations.TimeLogger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.DayOfWeek
import java.util.*

/**
 * Services for HeatingSchedule
 */
@Service
class HeatingScheduleServices @Autowired
constructor(private val heatingScheduleRepository: HeatingScheduleRepository, private val scheduleConverter: ScheduleConverter) {

    /**
     * Updates the schedule with the week provided.
     * The contains the index of the month that will be updated
     *
     * @param weeklyScheduleData    the new value for the week
     */
    @TimeLogger
    fun updateHeatingScheduleByWeek(weeklyScheduleData: WeekScheduleData): Boolean {
        val weekSchedule = heatingScheduleRepository.findWeekScheduleByMonth(weeklyScheduleData.month)

        if (weekSchedule != null) {
            weekSchedule.days = scheduleConverter.convertDayNetworkToDBDataMap(weeklyScheduleData.days)
            heatingScheduleRepository.save(weekSchedule)
            return true
        }

        return false
    }

    /**
     * Clears and initialises the schedule with all periods set to disabled
     *
     */
    @TimeLogger
    fun initialiseHeatingSchedule() {
        heatingScheduleRepository.deleteAll()

        val quarterSchedule = QuarterSchedule(false)

        val quarterScheduleMap = HashMap<String, QuarterSchedule>()

        // Quarters
        for (quarter in 0..3) {
            quarterScheduleMap[quarter.toString()] = quarterSchedule
        }

        val hourSchedule = HourSchedule(quarterScheduleMap)

        val hourScheduleMap = HashMap<String, HourSchedule>()

        // Hours
        for (hour in 0..23) {
            hourScheduleMap[hour.toString()] = hourSchedule
        }

        val daySchedule = DaySchedule(hourScheduleMap)

        val dayScheduleMap = HashMap<String, DaySchedule>()

        // Days
        for (day in DayOfWeek.values()) {
            dayScheduleMap[day.name] = daySchedule
        }

        var weekSchedule: WeekSchedule

        // Months
        for (month in 1..12) {
            weekSchedule = WeekSchedule(month, dayScheduleMap)

            heatingScheduleRepository.save(weekSchedule)
        }
    }

    /**
     * Updates the schedule for the provided day
     *
     * @param month           The index of the month to be updated
     * @param day             The day to be updated
     * @param dayScheduleData The new value for the day
     */
    @TimeLogger
    fun updateHeatingScheduleByDay(month: Int, day: String, dayScheduleData: DayScheduleData): Boolean {
        val weekSchedule = heatingScheduleRepository.findWeekScheduleByMonth(month)

        if (weekSchedule != null) {
            weekSchedule.days[day] = scheduleConverter.convertDayNetworkToDBData(dayScheduleData)
            heatingScheduleRepository.save(weekSchedule)
            return true
        }

        return false
    }

    /**
     * Gets the schedule identified by the provided month
     *
     * @param month The index of the month to be retrieved
     * @return      The schedule data for the month
     */
    @TimeLogger
    fun getScheduleByMonth(month: Int): Optional<WeekScheduleData> {
        val weekSchedule = heatingScheduleRepository.findWeekScheduleByMonth(month)

        return if (weekSchedule != null)
            Optional.of(scheduleConverter.convertWeekDBToNetworkData(weekSchedule))
        else
            Optional.empty()
    }

    /**
     * Gets the schedule data for provided day
     *
     * @param month The index of the month to be retrieved
     * @param day   The day to be retrieved
     * @return      The schedule data for the month
     */
    fun getScheduleByDay(month: Int, day: String): Optional<DayScheduleData> {
        val weekSchedule = heatingScheduleRepository.findWeekScheduleByMonth(month)

        return if (weekSchedule != null) {
            val daySchedule = weekSchedule.days[day]
            if (daySchedule != null) {
                Optional.of(scheduleConverter.convertDayDBToNetworkData(daySchedule))
            } else {
                Optional.empty()
            }
        } else
            Optional.empty()
    }
}