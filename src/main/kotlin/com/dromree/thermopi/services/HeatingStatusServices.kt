package com.dromree.thermopi.services

import com.dromree.thermopi.dbaccess.data.Accuracy
import com.dromree.thermopi.dbaccess.data.HeatingStatus
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.HeatingStatusRepository
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.PredictionValuesRepository
import com.dromree.thermopi.rest.data.HeatingStatusData
import com.dromree.thermopi.util.aspects.annotations.TimeLogger
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*

/**
 * Services for the HeatingStatus
 */
@Service
class HeatingStatusServices(private val heatingStatusRepository: HeatingStatusRepository,
                            private val predictionValuesRepository: PredictionValuesRepository,
                            private val targetTemperatureService: TargetTemperatureService,
                            private val temperatureRecordService: TemperatureRecordService) {

    /**
     * Gets the latest status of the heating
     *
     * @return  latest heating status
     */
    val latestHeatingStatus: Optional<HeatingStatusData>
        get() {
            val heatingStatus = heatingStatusRepository.findTopByOrderByStartTimeDesc()

            return if (heatingStatus != null)
                Optional.of(HeatingStatusData(heatingStatus.active))
            else
                Optional.empty()
        }

    /**
     * Saves the heating status provided in the database.
     * Populates with the current time.
     *
     * @param heatingStatusData HeatingStatus to be added
     */
    @TimeLogger
    fun setHeatingStatus(heatingStatusData: HeatingStatusData) {

        val heatingStatus = heatingStatusRepository.findTopByOrderByStartTimeDesc()
        val currentTemp = temperatureRecordService.currentTemperature
        val targetTemperature = targetTemperatureService.targetTemperature

        if (heatingStatus != null) {
            // if it is the same type do nothing? maybe updated time field?
            if (heatingStatusData.enabled == heatingStatus.active) {
                return
            } else {
                // set the end time to the current time
                heatingStatus.endTime = LocalDateTime.now()
                currentTemp.ifPresent { temp -> heatingStatus.endTemp = temp.recentAverage }

                if (heatingStatus.active
                        && currentTemp.isPresent
                        && targetTemperature.isPresent
                        && currentTemp.get().recentAverage >= targetTemperature.get().temperature) {
                    // compare the prediction to the end time and set the accuracy
                    heatingStatus.predictionAccuracy = getAccuracy(heatingStatus.prediction!!, heatingStatus.endTime!!)
                }

                heatingStatusRepository.save(heatingStatus)
            }
        }

        val nextPhase = HeatingStatus(heatingStatusData.enabled, false, LocalDateTime.now(), 0.0, 0.0, null, null, null, null)

        currentTemp.ifPresent { temperatureRecordData -> nextPhase.startTemp = temperatureRecordData.recentAverage }

        targetTemperature.ifPresent { targetTemperatureData -> nextPhase.targetTemp = targetTemperatureData.temperature }

        if (heatingStatusData.enabled) {
            // calculate prediction and set it
            val predictionValues = predictionValuesRepository.findFirstByOrderByCalculationDateDesc()

            if(predictionValues != null) {
                val tempDiffToTarget = nextPhase.targetTemp - nextPhase.startTemp

                val predictedMinutes = tempDiffToTarget * predictionValues.degreeVariable

                nextPhase.prediction = LocalDateTime.now().plusMinutes(predictedMinutes.toLong())
            }
        }

        heatingStatusRepository.save(nextPhase)
    }

    private fun getAccuracy(prediction: LocalDateTime, actual: LocalDateTime): Accuracy {
        val diffMinutes = actual.until(prediction, ChronoUnit.MINUTES)

        return when {
            diffMinutes < -15 -> Accuracy.MASSIVE_UNDERSHOT
            diffMinutes < -5 -> Accuracy.UNDERSHOT
            diffMinutes > 15 -> Accuracy.MASSIVE_OVERSHOT
            diffMinutes > 5 -> Accuracy.OVERSHOT
            else -> Accuracy.ON_TARGET
        }
    }
}