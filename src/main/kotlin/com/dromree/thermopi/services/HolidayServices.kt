package com.dromree.thermopi.services

import com.dromree.thermopi.dbaccess.data.Holiday
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.HolidayRepository
import com.dromree.thermopi.rest.data.HolidayData
import com.dromree.thermopi.util.aspects.annotations.TimeLogger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.*

/**
 * Services for Holidays
 */
@Service
class HolidayServices @Autowired
constructor(private val holidayRepository: HolidayRepository) {

    /**
     * Gets a list of all current and future holidays
     *
     * @return list of current and future holidays
     */
    val currentAndFutureHolidays: List<HolidayData>
        @TimeLogger
        get() {
            val holidaysList = holidayRepository.findHolidaysByEndDateGreaterThanEqualOrderByStartDateAsc(LocalDate.now())

            return if (holidaysList.isEmpty())
                emptyList()
            else
                convertDBToNetworkDataList(holidaysList)
        }

    /**
     * Gets a count of the currently active holidays
     *
     * @return  Count of active holidays
     */
    val currentHolidaysCount: Long
        get() =
            holidayRepository.countHolidaysByStartDateLessThanEqualAndEndDateGreaterThanEqual(LocalDate.now(), LocalDate.now())

    private fun convertDBToNetworkData(dbData: Holiday): HolidayData {
        return HolidayData(dbData.holidayId, dbData.startDate, dbData.endDate)
    }

    /**
     * Converts a list of db holidays to a list of network holidays
     *
     * @param dbList    List of holidays to be converted
     * @return          List of network holidays
     */
    private fun convertDBToNetworkDataList(dbList: List<Holiday>): List<HolidayData> {
        val networkList = ArrayList<HolidayData>()

        dbList.forEach { entry -> networkList.add(convertDBToNetworkData(entry)) }

        return networkList
    }

    /**
     * Gets the holiday identified by the holiday id
     *
     * @param holidayId id of the holiday to be retrieved
     * @return          holiday identified by the provided if, null if it is not found
     */
    fun getHolidayByHolidayId(holidayId: String): Optional<HolidayData> {
        val holiday = holidayRepository.findByHolidayId(holidayId)

        return if (holiday != null)
            Optional.of(convertDBToNetworkData(holiday))
        else
            Optional.empty()
    }

    /**
     * Updates the holiday provided
     *
     * @param aHolidayData  holiday to update
     */
    fun updateHoliday(aHolidayData: HolidayData) {
        var holiday = holidayRepository.findByHolidayId(aHolidayData.holidayId!!)

        if (holiday != null) {
            holiday.startDate = aHolidayData.startDate
            holiday.endDate = aHolidayData.endDate
        } else {
            holiday = Holiday(aHolidayData.holidayId!!, aHolidayData.startDate, aHolidayData.endDate)
        }

        holidayRepository.save(holiday)
    }

    /**
     * Adds the provided holiday to the database
     *
     * @param aHolidayData  holiday to be added
     */
    fun addHoliday(aHolidayData: HolidayData) {
        val holidayId = UUID.randomUUID().toString()
        aHolidayData.holidayId = holidayId

        val holiday = Holiday(aHolidayData.holidayId!!, aHolidayData.startDate, aHolidayData.endDate)

        holidayRepository.save(holiday)
    }

    /**
     * Deleted the holiday identified by the provided id from the database
     *
     * @param holidayId id of the holiday to be deleted
     */
    fun deleteHolidayByHolidayId(holidayId: String) {
        holidayRepository.deleteHolidayByHolidayId(holidayId)
    }
}
