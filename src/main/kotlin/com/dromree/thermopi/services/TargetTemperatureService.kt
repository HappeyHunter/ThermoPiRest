package com.dromree.thermopi.services

import com.dromree.thermopi.dbaccess.data.TargetTemperature
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.TargetTemperatureRepository
import com.dromree.thermopi.rest.data.TargetTemperatureData
import org.springframework.stereotype.Service
import java.util.*

/**
 * Services for TargetTemperature
 */
@Service
class TargetTemperatureService(private val targetTemperatureRepository: TargetTemperatureRepository) {

    /**
     * Gets the target temperature from the database
     *
     * @return  the target temperature
     */
    val targetTemperature: Optional<TargetTemperatureData>
        get() {
            val targetTemperatureList = targetTemperatureRepository.findAll()

            return if (targetTemperatureList.isEmpty())
                Optional.empty()
            else
                Optional.of(convertDBToNetworkData(targetTemperatureList[0]))
        }

    /**
     * Converts a db TargetTemperature to a network TargetTemperature
     *
     * @param dbData    TargetTemperature to be converted
     * @return          network TargetTemperature
     */
    private fun convertDBToNetworkData(dbData: TargetTemperature): TargetTemperatureData {
        return TargetTemperatureData(dbData.temperature)
    }

    /**
     * Updates the target temperature in the database
     *
     * @param targetTemperatureData The new target temperature
     */
    fun setTargetTemperature(targetTemperatureData: TargetTemperatureData) {
        val targetTemperatureList = targetTemperatureRepository.findAll()
        val targetTemperature: TargetTemperature

        if (!targetTemperatureList.isEmpty()) {
            targetTemperature = targetTemperatureList[0]
            targetTemperature.temperature = targetTemperatureData.temperature
        } else {
            targetTemperature = TargetTemperature(targetTemperatureData.temperature)
        }

        targetTemperatureRepository.save(targetTemperature)

    }
}