package com.dromree.thermopi.services

import com.dromree.thermopi.dbaccess.data.SensorReading
import com.dromree.thermopi.dbaccess.data.TemperatureRecord
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.TemperatureRecordRepository
import com.dromree.thermopi.rest.data.TemperatureRecordData
import com.dromree.thermopi.util.aspects.annotations.TimeLogger
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*

/**
 * Services for TemperatureRecords
 */
@Service
class TemperatureRecordService
constructor(private val temperatureRecordRepository: TemperatureRecordRepository,
            private val mongoTemplate: MongoTemplate) {

    /**
     * Gets the most recent temperature recorded.
     *
     * @return  The current temperature
     */
    val currentTemperature: Optional<TemperatureRecord>
        @TimeLogger
        get() {
            val temperatureRecord = temperatureRecordRepository.findFirstByOrderByYearDescMonthDescDayDesc()

            return if (temperatureRecord != null)
                Optional.of(temperatureRecord)
            else
                Optional.empty()
        }

    /**
     * Records the current temperature in the database
     *
     * @param currentData The current temperature
     */
    @TimeLogger
    fun recordCurrentTemperature(currentData: TemperatureRecordData) {
        val now = LocalDateTime.now()

        var temperatureRecord = temperatureRecordRepository.findFirstByYearAndMonthAndDay(now.year, now.monthValue, now.dayOfMonth)

        val sensorReading = SensorReading(currentData.temperature, currentData.humidity)

        if (temperatureRecord != null) {
            val maxTemp = if (temperatureRecord.maxTemp > currentData.temperature) temperatureRecord.maxTemp else currentData.temperature
            val minTemp = if (temperatureRecord.minTemp < currentData.temperature) temperatureRecord.minTemp else currentData.temperature

            val update = Update()
                    .set("minutes." + minuteOfDay(now), sensorReading)
                    .set("maxTemp", maxTemp)
                    .set("minTemp", minTemp)
                    .set("lastReadingTimestamp", LocalDateTime.now())
            mongoTemplate.updateFirst(Query(where("_id").`is`(temperatureRecord.id)), update, TemperatureRecord::class.java)
        } else {
            val minutesMap = HashMap<Int, SensorReading>()
            minutesMap[minuteOfDay(now)] = sensorReading
            temperatureRecord = TemperatureRecord(now.year,
                    now.monthValue,
                    now.dayOfMonth,
                    LocalDateTime.now(),
                    currentData.temperature,
                    0.0,
                    100.0,
                    minutesMap)
            temperatureRecordRepository.save(temperatureRecord)
        }
    }

    /**
     * Records the current temperature in the database
     *
     * @param id            The id of the log to update
     * @param recentAverage The new recent average temperature
     */
    @TimeLogger
    fun recordRecentAverage(id: String?, recentAverage: Double) {
        val update = Update()
                .set("recentAverage", recentAverage)
                .set("recentAverageTimestamp", LocalDateTime.now())
        mongoTemplate.updateFirst(Query(where("_id").`is`(id)), update, TemperatureRecord::class.java)
    }

    /**
     * Gets the current minute of the current day
     *
     * @param readingDateTime   LocalDateTime object to use
     * @return                  The current minute of the current day
     */
    private fun minuteOfDay(readingDateTime: LocalDateTime): Int {
        return readingDateTime.hour * 60 + readingDateTime.minute
    }
}
