package com.dromree.thermopi.services

import com.dromree.thermopi.dbaccess.data.ThermoPiUserDetails
import com.dromree.thermopi.dbaccess.mongodb.repositories.mongodb.ThermoPiUserDetailsRepository
import com.dromree.thermopi.rest.data.UserData
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Primary
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Primary
@Service
class ThermoPiUserDetailsService(private val userDetailsRepository: ThermoPiUserDetailsRepository, @param:Lazy private val passwordEncoder: PasswordEncoder) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {

        return userDetailsRepository.findFirstByUsername(username)
                ?: throw UsernameNotFoundException("No user found with username: $username")
    }

    fun addUser(userData: UserData): Optional<UserData> {
        val existingUser = userDetailsRepository.findFirstByUsername(userData.username)

        if (existingUser == null) {
            val user = ThermoPiUserDetails(
                    userData.username,
                    passwordEncoder.encode(userData.password),
                    false,
                    false,
                    false,
                    true,
                    userData.roles)

            userDetailsRepository.save(user)

            return Optional.of(userData)
        }

        return Optional.empty()
    }

    fun updatePassword(username: String, newPassword: String): Optional<String> {
        val existingUser = userDetailsRepository.findFirstByUsername(username)

        if (existingUser != null) {
            existingUser.setPassword(passwordEncoder.encode(newPassword))

            userDetailsRepository.save(existingUser)

            return Optional.of(username)
        }

        return Optional.empty()
    }
}
