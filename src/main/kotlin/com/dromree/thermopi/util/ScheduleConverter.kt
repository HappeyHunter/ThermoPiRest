package com.dromree.thermopi.util

import com.dromree.thermopi.dbaccess.data.DaySchedule
import com.dromree.thermopi.dbaccess.data.HourSchedule
import com.dromree.thermopi.dbaccess.data.QuarterSchedule
import com.dromree.thermopi.dbaccess.data.WeekSchedule
import com.dromree.thermopi.rest.data.DayScheduleData
import com.dromree.thermopi.rest.data.HourScheduleData
import com.dromree.thermopi.rest.data.QuarterScheduleData
import com.dromree.thermopi.rest.data.WeekScheduleData
import org.springframework.stereotype.Service
import java.util.*

/**
 * Converts schedule data between network and db objects
 */
@Service
class ScheduleConverter {

    // Begin Network to DB Conversions

    /**
     * Converts a network quarter to a db quarter
     *
     * @param networkData   network quarter to be converted
     * @return              db quarter
     */
    private fun convertQuarterNetworkToDBData(networkData: QuarterScheduleData): QuarterSchedule {
        return QuarterSchedule(networkData.enabled)
    }

    /**
     * Converts a map of network quarters to db quarters
     *
     * @param networkDataMap    map of network quarters to be converted
     * @return                  map of db quarters
     */
    private fun convertQuarterNetworkToDBDataMap(networkDataMap: MutableMap<String, QuarterScheduleData>): MutableMap<String, QuarterSchedule> {
        val quarterScheduleMap = HashMap<String, QuarterSchedule>()

        networkDataMap.forEach { key, value -> quarterScheduleMap[key] = convertQuarterNetworkToDBData(value) }

        return quarterScheduleMap
    }

    /**
     * Converts a network hour to a db hour
     *
     * @param networkData   network hour to be converted
     * @return              db hour
     */
    private fun convertHourDBToNetworkData(networkData: HourScheduleData): HourSchedule {
        return HourSchedule(convertQuarterNetworkToDBDataMap(networkData.quarters))
    }

    /**
     * Converts a map of network hours to db hours
     *
     * @param networkDataMap    map of network hours
     * @return                  map of db hours
     */
    private fun convertHourNetworkToDBDataMap(networkDataMap: MutableMap<String, HourScheduleData>): MutableMap<String, HourSchedule> {
        val hourScheduleMap = HashMap<String, HourSchedule>()

        networkDataMap.forEach { key, value -> hourScheduleMap[key] = convertHourDBToNetworkData(value) }

        return hourScheduleMap
    }

    /**
     * Converts a network day into a db day
     *
     * @param networkData   network day to be converted
     * @return              db day
     */
    fun convertDayNetworkToDBData(networkData: DayScheduleData): DaySchedule {
        return DaySchedule(convertHourNetworkToDBDataMap(networkData.hours))
    }

    /**
     * Converts a map of network days into a map of db days
     *
     * @param networkDataMap    map of network days
     * @return                  map of db days
     */
    fun convertDayNetworkToDBDataMap(networkDataMap: MutableMap<String, DayScheduleData>): MutableMap<String, DaySchedule> {
        val dayScheduleMap = HashMap<String, DaySchedule>()

        networkDataMap.forEach { key, value -> dayScheduleMap[key] = convertDayNetworkToDBData(value) }

        return dayScheduleMap
    }

    /**
     * Converts a network week into a db week
     *
     * @param networkData   network week to be converted
     * @return              db week
     */
    fun convertWeekNetworkToDBData(networkData: WeekScheduleData): WeekSchedule {
        return WeekSchedule(networkData.month, convertDayNetworkToDBDataMap(networkData.days))
    }

    // End Network to DB conversions

    // Begin DB to Network Conversions

    /**
     * Converts a db quarter to a network quarter
     *
     * @param dbData    db quarter to be converted
     * @return          network quarter
     */
    private fun convertQuarterDBToNetwork(dbData: QuarterSchedule): QuarterScheduleData {
        return QuarterScheduleData(dbData.enabled)
    }

    /**
     * Converts a map of db quarters to a map of network quarters
     *
     * @param dbDataMap map of db quarters to be converted
     * @return          map of network quarters
     */
    private fun convertQuarterDBToNetworkDataMap(dbDataMap: MutableMap<String, QuarterSchedule>): MutableMap<String, QuarterScheduleData> {
        val quarterScheduleDataMap = HashMap<String, QuarterScheduleData>()

        dbDataMap.forEach { key, value -> quarterScheduleDataMap[key] = convertQuarterDBToNetwork(value) }

        return quarterScheduleDataMap
    }

    /**
     * Converts a db hour to a network hour
     *
     * @param dbData    db hour to be converted
     * @return          network hour
     */
    private fun convertHourDBToNetworkData(dbData: HourSchedule): HourScheduleData {
        return HourScheduleData(convertQuarterDBToNetworkDataMap(dbData.quarters))
    }

    /**
     * Converts a map of db hours to a map of network hours
     *
     * @param dbDataMap map of db hours to be converted
     * @return          map of network hours
     */
    private fun convertHourDBToNetworkDataMap(dbDataMap: MutableMap<String, HourSchedule>): MutableMap<String, HourScheduleData> {
        val hourScheduleDataMap = HashMap<String, HourScheduleData>()

        dbDataMap.forEach { key, value -> hourScheduleDataMap[key] = convertHourDBToNetworkData(value) }

        return hourScheduleDataMap
    }

    /**
     * Converts a db day to a network day
     *
     * @param dbData    db day to be converted
     * @return          network day
     */
    fun convertDayDBToNetworkData(dbData: DaySchedule): DayScheduleData {
        return DayScheduleData(convertHourDBToNetworkDataMap(dbData.hours))
    }

    /**
     * Converts a map of db days to a map of network days
     *
     * @param dbDataMap map of db days to be converted
     * @return          map of network days
     */
    private fun convertDayDBToNetworkDataMap(dbDataMap: MutableMap<String, DaySchedule>): MutableMap<String, DayScheduleData> {
        val dayScheduleDataMap = HashMap<String, DayScheduleData>()

        dbDataMap.forEach { key, value -> dayScheduleDataMap[key] = convertDayDBToNetworkData(value) }

        return dayScheduleDataMap
    }

    /**
     * Converts a db week to a network week
     *
     * @param dbData    db week to be converted
     * @return          network week
     */
    fun convertWeekDBToNetworkData(dbData: WeekSchedule): WeekScheduleData {
        return WeekScheduleData(dbData.month, convertDayDBToNetworkDataMap(dbData.days))
    }

    // End DB to Network conversions - Move to converter class
}