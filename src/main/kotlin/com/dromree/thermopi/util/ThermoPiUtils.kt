package com.dromree.thermopi.util

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

object ThermoPiUtils {
    fun localToZoned(localDateTime: LocalDateTime?): ZonedDateTime? {
        return if (localDateTime != null)
            ZonedDateTime.of(localDateTime, ZoneId.systemDefault())
        else
            null
    }

    fun zonedToLocal(zonedDateTime: ZonedDateTime?): LocalDateTime? {
        return zonedDateTime?.toLocalDateTime()
    }

    fun isInFuture(localDateTime: LocalDateTime): Boolean {
        return LocalDateTime.now().isBefore(localDateTime)
    }

    fun isInFuture(zonedDateTime: ZonedDateTime): Boolean {
        return isInFuture(zonedDateTime.toLocalDateTime())
    }
}