package com.dromree.thermopi.util.aspects

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Aspect
@Component
class TimeLoggerAspect {

    @Around("@annotation(com.dromree.thermopi.util.aspects.annotations.TimeLogger)")
    @Throws(Throwable::class)
    fun timeLog(joinPoint: ProceedingJoinPoint): Any? {
        val start = System.currentTimeMillis()

        val proceed = joinPoint.proceed()

        val executionTime = System.currentTimeMillis() - start

        logger.debug("{} executed in {}ms", joinPoint.signature, executionTime)
        return proceed
    }

    companion object {

        private val logger = LoggerFactory.getLogger(TimeLoggerAspect::class.java.name)
    }
}