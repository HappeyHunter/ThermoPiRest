package com.dromree.thermopi.util.aspects.annotations

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER)
@Retention
annotation class TimeLogger